![logo_with_text](https://bitbucket.org/elaplace/tulips/raw/153ff99cad7cf514414969b85895892f05ee492a/tulips/logo/Tulips_acronym_text_transparent.png)

[![Documentation Status](https://readthedocs.org/projects/astro-tulips/badge/?version=latest)](https://astro-tulips.readthedocs.io/en/latest/?badge=latest)
[![PyPI version](https://badge.fury.io/py/astro-tulips.svg)](https://badge.fury.io/py/astro-tulips)
[![ASCL badge](https://img.shields.io/badge/ascl-2110.004-blue.svg?colorB=262255)](https://ascl.net/2110.004) 
[![arXiv badge](https://img.shields.io/badge/arxiv-2111.05346-red)](https://arxiv.org/abs/2111.05346) 

`tulips` creates diagrams of the structure and evolution of stars. It creates plots and movies based on output from 
the [MESA](http://mesa.sourceforge.net/) stellar evolution code. 
`tulips` represents stars as circles of varying size and color. 
`tulips`' capabilities include visualizing the size and perceived color of stars, their interior 
mixing and nuclear burning processes, their chemical composition, and comparing different MESA models.
![example_gif](https://bitbucket.org/elaplace/tulips/raw/153ff99cad7cf514414969b85895892f05ee492a/tulips/MESA_DIR_EXAMPLE/test_single_M10.5_movie.gif "Example mearings movie showing the evolution of 
a 10Msun star")

## Installation
#### General
To install tulips run
```
pip install astro-tulips
```
For more information, please have a look at the dedicated 
[installage page](https://astro-tulips.readthedocs.io/en/latest/installation.html) of the tulips documentation.

**Note**: Python 2 is not supported

#### Development version
The latest development version is available directly at [bitbucket](https://bitbucket.org/elaplace/tulips)
```
git clone https://bitbucket.org/elaplace/tulips.git
cd tulips
pip install -e .
```

## Documentation
More details, examples, and tutorials can be found in the tulips [documentation](https://astro-tulips.readthedocs.io)

## Citing TULIPS
The paper *TULIPS: a Tool for Understanding the Lives, Interiors, and Physics of Stars* has been accepted for
publication in Astronomy & Computing. 
[![arXiv badge](https://img.shields.io/badge/arxiv-2111.05346-red)](https://arxiv.org/abs/2111.05346) 

**If you use TULIPS for your research, please cite this article**.
  
## License
This work is distributed under a GNU general public license, version 3.

## Contributing
If you wish to submit a new feature or bug fix please don't hesitate to send a pull request or to submit an issue.

## Acknowledgments
tulips makes use of the open-source python modules [mesaPlot](https://github.com/rjfarmer/mesaplot) by R. Farmer, 
[colorpy](http://markkness.net/colorpy/ColorPy.html) by M. Kness, [CMasher](https://cmasher.readthedocs.io), [numpy](https://numpy.org/), 
[astropy](https://www.astropy.org/),[matplotlib](https://matplotlib.org/), and of [ipython/jupyter](https://jupyter.org).
Logo design: A. Faber. Documentation: I. de Langen. Created and developed by [Eva Laplace](https://evalaplace.github.io/).

## Funding
This project was funded by the European Unions Horizon 2020
research and innovation program from the European Research
Council (ERC, grant agreement No.715063) and the Netherlands
Organisation for Scientific Research (NWO) as part of the Vidi
research program BinWaves with project number 639.042.728.
The [ET Outreach award](https://khmw.nl/et-outreach-award/) of the Royal Holland Society of Sciences and Humanities and its
sponsors E. van Dishoek and T. de Zeeuw are gratefully acknowledged for
making it possible to fund the work of I. de Langen on the TULIPS
documentation and tutorials.