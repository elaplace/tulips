{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Perceived color diagram\n",
    "\n",
    "In this notebook we will demonstrate how to create one of the possible `tulips` diagrams. The perceived color diagram is useful when you want to display a property that applies for the entire star, like radius and color. `tulips` visualizes the star as a circle, where its radius represents the actual radius, or any other physical quantity that you're interested in. This circle is filled with a color, which indicates the temperature of the star, according to how it would appear on the sky. Therefore, the diagram is an approximation of how the star would be perceived by the human eye. \n",
    "\n",
    "### Load example model\n",
    "Let's plot a diagram of a $11 M_{\\odot}$ star at the beginning of its evolution. As explained in the [Getting Started](getting_started.ipynb#Loading-a-MESA-model) page, we first need to load in a MESA model with `mesaPlot` and store it in an object called `m11`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interactive matplotlib plotting for jupyter lab\n",
    "%matplotlib inline\n",
    "\n",
    "# If you use jupyter notebook\n",
    "# %matplotlib notebook\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import mesaPlot as mp\n",
    "import tulips\n",
    "\n",
    "# Specify  directory of MESA model\n",
    "SINGLE_M11_DIR = \"../../tulips/MESA_DIR_EXAMPLE/\"\n",
    "\n",
    "m11 = mp.MESA()\n",
    "m11.loadHistory(filename_in=SINGLE_M11_DIR + \"history.data\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# # Use LaTeX in the plots\n",
    "col_w = 3.5    # Figure width in inches, columnswidth\n",
    "text_w = 7.25 # Full page figure width\n",
    "plt.rcParams.update({\n",
    "    'figure.figsize': (col_w, col_w/(4/3.)),     # 4:3 aspect ratio\n",
    "    'font.size' : 11,                   # Set font size to 11pt\n",
    "    'axes.labelsize': 11,               # -> axis labels\n",
    "    'legend.fontsize': 9,              # -> legends\n",
    "    'font.family': 'lmodern',\n",
    "    'text.usetex': True,\n",
    "    'text.latex.preamble': (            # LaTeX preamble\n",
    "        r'\\usepackage{lmodern}'\n",
    "        # ... more packages if needed\n",
    "    ),\n",
    "    'figure.dpi': 200,\n",
    "})\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create perceived color plot\n",
    "Now we are ready to create the perceived color diagram of this star. We can do this by passing the MESA object to the `perceived_color` function. Let's plot an instance of the star at `time_ind=0`. (Have a look at the [Getting Started](getting_started.ipynb#Creating-your-first-tulips-diagram) page on how to specify this time parameter) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=0, fig_size=(8, 6))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The radius of the star is shown on the x and y axis. Its blue color shows how it would be perceived by the human eye, and indicates a relatively hot star."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "    \n",
    "The color is determined using the `colorpy` package, which approximates the spectrum of a star using its effective temperature. According to the 1931 color matching functions of the Commission Internationale de l’Eclairage, this spectrum is converted to the approximate RGB color the human eye will perceive. \n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create an animation\n",
    "To see the star evolving, and observe its changing radius and color, we can also create an animation. To do this, we need to modify the `time_ind` property. Here, we want to plot from the first (0) to last (-1) time index, with steps of 10. For more information about the `time_ind` property, look at the [Getting Started](getting_started.ipynb) page. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=(0, -1, 10), fps=30, output_fname=\"perceived_color\", fig_size=(8, 6))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"perceived_color.mp4\", embed=True, width=700, height=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Showing other properties\n",
    "Next to showing the star's size, we can use this diagram to visualize other properties. It is possible to specify which quantity the radius is representing. This can be done by changing the `raxis` property to any other quantity that is stored in the MESA output file. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Tip\n",
    "    \n",
    "To access all the different properties, you can type `m11.hist.` and press `Tab`. \n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example below, we change this property to `star_mass`, to show the mass instead of radius on the x and y axis. A larger circle now represents a more massive star and vice versa. The color still shows the perceived color. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=0, raxis='star_mass', fig_size=(8, 6))\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
