Installation
=============

The easiest way to install ``tulips`` and all its dependencies is through pip::

    pip install astro-tulips

**Note:** Python 2 is not supported

Installing from source
^^^^^^^^^^^^^^^^^^^^^^^
To install from source, git clone the directory, then install::

    git clone https://bitbucket.org/elaplace/tulips.git
    cd tulips
    python3 setup.py install

The latest development version is available on the ``development`` branch. To create a local installation call::

    pip install -e .

Additional dependencies
^^^^^^^^^^^^^^^^^^^^^^^
You need to have mesaPlot installed for reading MESA output files (see `mesaPlot <https://github.com/rjfarmer/mesaplot>`_).
``tulips`` relies on the ``ffmpeg`` software. It is automatically installed through the ``imageio_ffmpeg`` package.
You also need to have Latex installed to properly display the diagrams. The easiest way to do this is by installing `TexLive <https://www.tug.org/texlive/acquire-netinstall.html>`_.

Mencoder
""""""""
The ``mencoder`` software may be needed on some platforms for creating the movies. To install ``mencoder`` on Windows,
you can download `MPlayer <http://mplayerwin.sourceforge.net/downloads.html>`_. On Ubuntu, you can install ``mencoder`` via::

    sudo apt install mencoder



.. warning::
    These additional dependencies are crucial. Without them, you will not be able to create TULIPS animations.

Running on Windows
^^^^^^^^^^^^^^^^^^

When getting ``tulips`` installed on Windows, it is important to add the dependencies to your system environment variable 'Path'.
You can do this by going to Start > Settings > Info > Advanced > Environment Variables. Here you can add a new
variable to 'Path' in the System Variables. This variable should contain the path to the installed program.

.. note::
    You can check if you installed the program (e.g. ``mencoder``) with ``<program_name> -version``. This should return the
    version of the program.
