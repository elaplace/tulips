{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Timestep options \n",
    "\n",
    "When creating an animation, `tulips` offers multiple options to rescale the time in order to capture all evolutionary phases.  This is controlled by the `time_scale_type` argument, for which three different options exist: `model_number`, `linear` and `log_to_end`. In this notebook we demonstrate these possibilities. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    \n",
    "Note\n",
    "\n",
    "The `time_scale_type` option is only available for the `perceived_color` and `energy_and_mixing` diagrams. It requires many output models to be stored, which is not always the case for diagrams that rely on MESA profile output. \n",
    "\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### model_number\n",
    "The default option `model_number` is used for the animations in the tutorials. In this case, the timesteps follow the moments when a new MESA model was saved. This is a non-physical arbitrary timescale that depends on various resolution settings in MESA. We create a `energy_and_mixing` animation with this setting for a 11 $M_{\\odot}$ star: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interactive matplotlib plotting for jupyter lab\n",
    "%matplotlib inline\n",
    "\n",
    "# If you use jupyter notebook\n",
    "# %matplotlib notebook\n",
    "\n",
    "import matplotlib as plt\n",
    "import mesaPlot as mp\n",
    "import tulips\n",
    "\n",
    "EXAMPLE_DIR = \"../../tulips/MESA_DIR_EXAMPLE/\"\n",
    "\n",
    "m = mp.MESA() # Create MESA object\n",
    "m.loadHistory(filename_in=EXAMPLE_DIR + 'history.data')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# # Use LaTeX in the plots\n",
    "\n",
    "col_w = 3.5    # Figure width in inches, columnswidth\n",
    "text_w = 7.25 # Full page figure width\n",
    "plt.rcParams.update({\n",
    "    'figure.figsize': (col_w, col_w/(4/3.)),     # 4:3 aspect ratio\n",
    "    'font.size' : 11,                   # Set font size to 11pt\n",
    "    'axes.labelsize': 11,               # -> axis labels\n",
    "    'legend.fontsize': 9,              # -> legends\n",
    "    'font.family': 'lmodern',\n",
    "    'text.usetex': True,\n",
    "    'text.latex.preamble': (            # LaTeX preamble\n",
    "        r'\\usepackage{lmodern}'\n",
    "        # ... more packages if needed\n",
    "    ),\n",
    "    'figure.dpi': 200,\n",
    "})\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m, time_ind=(0,-1,10), fps=30, time_scale_type='model_number', output_fname='em_model_number')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"em_model_number.mp4\", embed=True, width=700, height=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### linear\n",
    "Alternatively, when you set `time_scale_type='linear'`, the timesteps follow the actual physical age of the star (linearly). During the lifetime of a star, it passes through different stages of stellar evolution. These stages can have vastly different timescales. Stars spend the longest part of their life on the main sequence, whereas the last stages of nuclear burning can last for only a day. When we produce an animation with this setting, the majority of the resulting video therefore shows the star slowly evolving on the main sequence: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m, time_ind=(0,-1,10), fps=30, time_scale_type='linear', output_fname='em_linear')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"em_linear.mp4\", embed=True, width=700, height=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### log_to_end\n",
    "To capture the shorter phases at the end of the evolution of a stellar model, you can rescale the time in such a way that all evolution stages have more similar durations. You can do this by setting `time_scale_type='log_to_end'`. When this is set, the time follows $\\textrm{log}_{10}(t_{\\textrm{end}} - t)$, where $t_{\\textrm{end}}$ is the final age of the stellar model and $t$ is the age of the star. As a result, the earlier phases shorten whereas the later phases lengthen. This allows us to distinguish the different evolutionary stages and also capture the fast transitions. Especially for the `energy_and_mixing` diagram this is useful since it reveals the different stages of nuclear burning in more detail. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m, time_ind=(0,-1,10), fps=15, time_scale_type='log_to_end', output_fname='em_log_to_end')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"em_log_to_end.mp4\", embed=True, width=700, height=600)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Metagegevens bewerken",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
