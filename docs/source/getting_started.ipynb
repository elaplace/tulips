{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting started\n",
    "\n",
    "Here, we will explain the basics that are needed to get started with `tulips`. `tulips` creates visualizations of stellar evolution based on output from the [MESA](http://mesa.sourceforge.net/) stellar evolution code. To read MESA output files, `tulips` uses the [mesaPlot](https://github.com/rjfarmer/mesaplot) package. Here we will show you how to load a MESA model and how you can explore its properties. We will use jupyter notebooks to show the tutorials. With this application you can edit and run python code via a web browser. Have a look [here](https://jupyterbook.org/intro.html) for information on how to download and use it. \n",
    "\n",
    "### Loading a MESA model \n",
    "To show the functionalities of the `mesaPlot` output, we use a pre-computed MESA stellar model of an $11  M_{\\odot}$ star. In order to explore the properties of this model, we can create a `mesaPlot` object called `m11`, in which all the information about the stellar model will be stored. \n",
    "MESA produces two types of output files:\n",
    "\n",
    "* `history.data` files that contain the evolution of one-dimensional properties of the stellar model, such as its radius.\n",
    "* `profile.data` files that contain a snapshot of the interior stellar structure at a particular moment in time. For example, this file can contain the temperature as a function of the radius/mass coordinate\n",
    "\n",
    "In the example below, we show how to load the output of a MESA history file with `mesaPlot`, after which we can access its properties."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "Note\n",
    "\n",
    "Example MESA models, including the one presented here, are available on the TULIPS zenodo repository \n",
    "</div>\n",
    "\n",
    "[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5032250.svg)](https://doi.org/10.5281/zenodo.5032250)\n",
    "\n",
    " \n",
    "In the example notebooks we use the model of the evolution of an 11 solar mass star at solar metallicity from core hydrogen burning until the end of core oxygen burning (saved on zenodo under single_11Msun.tar.gz)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interactive matplotlib plotting for jupyter lab\n",
    "%matplotlib inline\n",
    "\n",
    "# If you use jupyter notebook\n",
    "# %matplotlib notebook\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import mesaPlot as mp\n",
    "\n",
    "EXAMPLE_DIR = \"../../tulips/MESA_DIR_EXAMPLE/\"\n",
    "\n",
    "m11 = mp.MESA() # Create MESA object\n",
    "m11.loadHistory(filename_in=EXAMPLE_DIR + 'history.data')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# # Use LaTeX in the plots\n",
    "\n",
    "col_w = 3.5    # Figure width in inches, columnswidth\n",
    "text_w = 7.25 # Full page figure width\n",
    "plt.rcParams.update({\n",
    "    'figure.figsize': (col_w, col_w/(4/3.)),     # 4:3 aspect ratio\n",
    "    'font.size' : 11,                   # Set font size to 11pt\n",
    "    'axes.labelsize': 11,               # -> axis labels\n",
    "    'legend.fontsize': 9,              # -> legends\n",
    "    'font.family': 'lmodern',\n",
    "    'text.usetex': True,\n",
    "    'text.latex.preamble': (            # LaTeX preamble\n",
    "        r'\\usepackage{lmodern}'\n",
    "        # ... more packages if needed\n",
    "    ),\n",
    "    'figure.dpi': 200,\n",
    "})\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Tip\n",
    "\n",
    "To copy all lines of code in a cell, you can just click on the copy button in the upper right corner!\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing properties\n",
    "After the history output file has been loaded, properties such as the mass, age or radius of the star can be accessed through the `hist` property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "star_age = m11.hist.star_age # Access star age\n",
    "log_R = m11.hist.log_R # Access star log radius"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to have a look at the different properties that are stored, type `m11.hist.` and press `Tab`. This will show you a list of the information that can be accessed. You can use these properties to make simple plots. For extensive documentation about `mesaPlot`, have a look at the [mesaPlot](https://github.com/rjfarmer/mesaplot) GitHub page. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating your first tulips diagram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have explored the basic properties of our star, we are ready to visualize it using `tulips`! `tulips` is capable of generating different kinds of diagrams, which are shown at the [Homepage](index.rst#Tulips-diagrams). For demonstration, we will plot one of these diagrams: the perceived color diagram. This can be done using just one command, passing our MESA object `m11` to the `perceived_color` function from `tulips`. It will show us the perceived color and size of the star at a single point in time, specified by the time index `time_ind`. For now, we only want to plot at `time_ind = 0`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tulips \n",
    "\n",
    "tulips.perceived_color(m11, time_ind=0, fig_size=(8,6))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is your first `tulips` plot! We see a blue $10.5 M_{\\odot}$ star. The x and y axis show the radius of the star. The color represents the temperature of the star in the same way we would observe it: blue is hot, red is cooler. On the bottom left we see an inset Herzsprung Russel diagram, showing that this star is just at the beginning of the main sequence. We can use this first diagram to verify if everything looks fine, before moving on to making an animation. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "\n",
    "`time_ind` refers to an index that follows every model stored in a MESA `history.data` file. This means the first model is at index 0 and the last is at index -1. It depends on your model which age this corresponds to. The powerful thing about `time_ind` is that it lets you find out some specific times very easily through numpy functions. For example, if you want to create a model of a star at the point when it finishes hydrogen burning, you can look for the model where the central hydrogen mass fraction is very small, like `end_hburn_index = np.where(m11.hist.center_h1 < 1e-4)[0][0]`. \n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating your first tulips animation \n",
    "\n",
    "Once you are happy with it, it would be interesting to see how the star is evolving and how that reflects in its appearance. Therefore, we can create an animation of the evolution of the star. To view the animation, we first have to save it. Afterwards we can open it with the `Video` function from the `IPython` library. It is not possible to see the animation without saving it. Let's create an animation of our stellar model:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": {
     "execute": "never"
    },
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=(0, -1, 10), fps=30, output_fname=\"perceived-color-test\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"perceived-color-test.mp4\", embed=True, width=700, height=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, `time_ind` contains the start index, the end index, and the time step. The `fps` keyword (which stands for \"frames per second\") helps control the speed of the resulting video. More information and all the options can be found in the function docstring and in the `tulips` API. The function docstring can be found by typing `tulips.perceived_color()` and press `Shift` + `Tab` inside the parenthesis. The animation is saved as .mp4 file in a directory, which you can specify with `output_fname`. You can then call the `Video` function with the specified name. We can now see the star changing over time in radius, color and mass while it follows its track on the Herzsprung Russel diagram. "
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Metagegevens bewerken",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
