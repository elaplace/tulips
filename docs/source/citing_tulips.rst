Citing TULIPS
=============

The paper *TULIPS: a Tool for Understanding the Lives, Interiors, and Physics of Stars* has been published in
the journal `Astronomy & Computing <https://doi.org/10.1016/j.ascom.2021.100516>`_.

**If you use tulips for your research, please cite this article**.

Bibcode::

  @ARTICLE{2022A&C....3800516L,
           author = {{Laplace}, E.},
           title = "{TULIPS: A Tool for Understanding the Lives, Interiors, and Physics of Stars}",
           journal = {Astronomy and Computing},
           keywords = {Stars General, Stars Evolution, Visualization - python, Astrophysics - Instrumentation and Methods for Astrophysics, Astrophysics - Solar and Stellar Astrophysics},
           year = 2022,
           month = jan,
           volume = {38},
           eid = {100516},
           pages = {100516},
           doi = {10.1016/j.ascom.2021.100516},
           archivePrefix = {arXiv},
           eprint = {2111.05346},
           primaryClass = {astro-ph.IM},
           adsurl = {https://ui.adsabs.harvard.edu/abs/2022A&C....3800516L},
           adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }

**Optionally**, the code itself can also be referenced through the Astrophysics Source Code Library.

|ascl:2110.004|

.. |ascl:2110.004| image:: https://img.shields.io/badge/ascl-2110.004-blue.svg?colorB=262255
   :target: https://ascl.net/2110.004

Bibcode::

  @MISC{2021ascl.soft10004L,
        author = {{Laplace}, Eva},
        title = "{TULIPS: Tool for Understanding the Lives, Interiors, and Physics of Stars}",
        keywords = {Software},
        year = 2021,
        month = oct,
        eid = {ascl:2110.004},
        pages = {ascl:2110.004},
        archivePrefix = {ascl},
        eprint = {2110.004},
        adsurl = {https://ui.adsabs.harvard.edu/abs/2021ascl.soft10004L},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
   }
