# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import inspect

sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'tulips'
copyright = '2021, Eva Laplace'
author = 'Eva Laplace'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx_rtd_theme',
              'sphinx_automodapi.automodapi',
              'sphinx_automodapi.smart_resolver',
              'numpydoc',
              'nbsphinx',
              'sphinx.ext.mathjax',
              'sphinx_copybutton',
              'sphinx_gallery.load_style',
              'matplotlib.sphinxext.plot_directive',
              'IPython.sphinxext.ipython_console_highlighting',
              'IPython.sphinxext.ipython_directive',
              'sphinx.ext.linkcode'
              ]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
# exclude_patterns = ['energy_and_mixing_diagram.ipynb']

numpydoc_show_class_members = False

# Add a heading to notebooks
nbsphinx_prolog = """
{% set docname = env.doc2path(env.docname, base=None) %}
.. note:: This tutorial was generated from a Jupyter notebook that can be
          downloaded `here <https://bitbucket.org/elaplace/tulips/src/master/docs/source/{{ docname }}>`_.
.. raw:: html
    <style>
        .nbinput .prompt,
        .nboutput .prompt {
            display: none;
        }
    </style>
"""

nbsphinx_prompt_width = "0"

nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc=figure.dpi=96",
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'style_external_links': True,
}

html_favicon = 'figures/tulips_icon.ico'
html_logo = "figures/tulips_logo.png"


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_css_files = ["custom.css"]
html_js_files = ['custom.js']

nbsphinx_allow_errors = True

def linkcode_resolve(domain, info):
    """function for linkcode sphinx extension"""
    def find_func():
        # find the installed module in sys module
        sys_mod = sys.modules[info["module"]]

        # use inspect to find the source code and starting line number
        func = getattr(sys_mod, info["fullname"])
        source_code, line_num = inspect.getsourcelines(func)

        # get the file name from the module
        file = info["module"].split(".")[-1]

        return file, line_num, line_num + len(source_code) - 1

    # ensure it has the proper domain and has a module
    if domain != 'py' or not info['module']:
        return None

    # attempt to cleverly locate the function in the file
    try:
        file, start, end = find_func()
        # stitch together a github link with specific lines
        filename = "tulips/{}.py#lines-{}:{}".format(file, start, end)

    # if you can't find it in the file then just link to the correct file
    except Exception:
        filename = info['module'].replace('.', '/') + '.py'
    return "https://bitbucket.org/elaplace/tulips/src/master/{}".format(filename)