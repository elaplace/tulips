{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Energy and mixing diagram\n",
    "\n",
    "In this notebook we will discuss how to create one of the possible `tulips` diagrams. The energy and mixing diagrams gives an overview about the energy generation rates thoughout a the stellar model. Moreover, it provides information about the mixing processes occuring in the interior. In this diagram, `tulips` visualizes the stellar model as a circle, where its radius represents the total mass of the star. \n",
    "\n",
    "### Load example model\n",
    "Let's plot an energy and mixing diagram of a $11 M_{\\odot}$ star. As explained in the [Getting Started](getting_started.ipynb#Loading-a-MESA-model) page, we first need to load in the history output of a MESA model with `mesaPlot` and store it in an object called `m11`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interactive matplotlib plotting for jupyter lab\n",
    "%matplotlib inline\n",
    "\n",
    "# If you use jupyter notebook\n",
    "# %matplotlib notebook\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import mesaPlot as mp\n",
    "import tulips\n",
    "\n",
    "# Specify directory of MESA model\n",
    "SINGLE_M11_DIR = \"../../tulips/MESA_DIR_EXAMPLE/\"\n",
    "\n",
    "m11 = mp.MESA()\n",
    "m11.loadHistory(f=SINGLE_M11_DIR)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# # Use LaTeX in the plots\n",
    "col_w = 3.5    # Figure width in inches, columnswidth\n",
    "text_w = 7.25 # Full page figure width\n",
    "plt.rcParams.update({\n",
    "    'figure.figsize': (col_w, col_w/(4/3.)),     # 4:3 aspect ratio\n",
    "    'font.size' : 11,                   # Set font size to 11pt\n",
    "    'axes.labelsize': 11,               # -> axis labels\n",
    "    'legend.fontsize': 9,              # -> legends\n",
    "    'font.family': 'lmodern',\n",
    "    'text.usetex': True,\n",
    "    'text.latex.preamble': (            # LaTeX preamble\n",
    "        r'\\usepackage{lmodern}'\n",
    "        # ... more packages if needed\n",
    "    ),\n",
    "    'figure.dpi': 200,\n",
    "})\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create an energy diagram\n",
    "Then we create an instance of the stellar model at `time_ind=2200` as follows: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m11, time_ind=2200, cmin=-10, cmax=10, show_total_mass=True, fig_size=(8,6))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The stellar model is shown as a circle, whose radius represents the total mass (however, this can be changed by modifying the `raxis` property). The colors inside the star represents the energy generation rate. As indicated by the colorbar on the right, yellow to red colors are used for a positive energy generation rate, caused by nuclear burning processes. As we can see in the inset HR diagram, we are looking at a star in the later stages of its evolution. We can observe burning in a shell, as well as a burning region in its core. The colors can also show energy loss by neutrino emission, which are shown in purple. You can hide the colorbar by setting `show_colorbar` to `False`. To see how you can change the colormap, have a look at the [Customize options](customize.ipynb#Colormap) page. The label of the colorbar can be changed with the `cbar_label` option. It is possible to put limits on the burning rates using the `cmin` and `cmax` property. In the example above we used the default values. However, if you are for example only interested in energy generation and not in cooling, you could change `cmin` to 0 to only include positive energy generation rates. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "    \n",
    "The energy generation rate $\\epsilon$ is calculated from the difference between nuclear burning rate and neutrino energy as follows:  \n",
    "$\\epsilon = \\textrm{sign}(\\epsilon_{nuc} - \\epsilon_{\\nu})\\textrm{log}_{10}(\\textrm{max}(1.0,|\\epsilon_{nuc}-\\epsilon{\\nu}|)/[\\textrm{erg g}^{-1}\\textrm{s}^{-1}])$ \n",
    "    \n",
    "Important here is to notice that this is a logarithmic quantity.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create an energy and mixing diagram\n",
    "Next to visualizing the regions where energy generation or losses occur, `tulips` can also show the dominant mixing process in the interior. We can add this information to the diagram by changing the `show_mix` and `show_mix_legend` variables to `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m11, time_ind=0, cmin=-10, cmax=10, show_total_mass=True, \n",
    "                         show_mix=True, show_mix_legend=True, fig_size=(8,6))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to the energy generation rates, we now also see areas with grey patterns indicating the mixing process. Which patterns correspond to the different types of mixing processes can be seen in the legend. The mixing hatches can be customized by changing the `tulips.MIX_HATCHES` variable. This stellar model has a convective core with overshooting at the edge. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating an animation \n",
    "We can create an animation of our energy and mixing diagram by changing the `time_ind` property. You can specify the start index, end index and timestep. In the following example we choose to show the first (0) to last (-1) model, with steps of 10. `fps` (frames per second) controls the speed of the video. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [],
   "source": [
    "tulips.energy_and_mixing(m11, time_ind=(0,-1, 10), fps=30, cmin=-10, cmax=10, fig_size=(8,6)\n",
    "                         show_total_mass=True, show_mix=True, show_mix_legend=True, output_fname=\"energy_and_mixing\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"energy_and_mixing.mp4\", embed=True, width=700, height=600)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
