{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Customize options \n",
    " \n",
    "All `tulips` diagrams are customizable and contain many options that allow you to change the plots to your liking. These options are managed by a set of keywords that can be specified when calling a `tulips` function. In this notebook we mention the main possibilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# Interactive matplotlib plotting for jupyter lab\n",
    "%matplotlib inline\n",
    "\n",
    "# If you use jupyter notebook\n",
    "# %matplotlib notebook\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import mesaPlot as mp\n",
    "import tulips\n",
    "\n",
    "EXAMPLE_DIR = \"../../tulips/MESA_DIR_EXAMPLE/\"\n",
    "\n",
    "m11 = mp.MESA() # Create MESA object\n",
    "m11.loadHistory(filename_in=EXAMPLE_DIR + 'history.data')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "# # Use LaTeX in the plots\n",
    "\n",
    "col_w = 3.0    # Figure width in inches, columnswidth\n",
    "text_w = 7.25 # Full page figure width\n",
    "plt.rcParams.update({\n",
    "    'figure.figsize': (col_w, col_w/(4/3.)),     # 4:3 aspect ratio\n",
    "    'font.size' : 11,                   # Set font size to 11pt\n",
    "    'axes.labelsize': 11,               # -> axis labels\n",
    "    'legend.fontsize': 9,              # -> legends\n",
    "    'font.family': 'lmodern',\n",
    "    'text.usetex': True,\n",
    "    'text.latex.preamble': (            # LaTeX preamble\n",
    "        r'\\usepackage{lmodern}'\n",
    "        # ... more packages if needed\n",
    "    ),\n",
    "    'figure.dpi': 200,\n",
    "})\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Options available for all TULIPS diagrams\n",
    "\n",
    "##### Changing the axis and label\n",
    "The `raxis` property indicates what is represented by the radius of the circle. This is the quantity that is specified by the x and y axis labels. You can change the label of the x and y axis with the `axis_label` option. \n",
    "\n",
    "##### Figure options\n",
    "It is possible to plot `tulips` diagrams on already existing `matplotlib` `fig` and `ax` objects. This can be useful if you want to create subplots with multiple diagrams. You can pass these objects to the function with the `fig` and `ax` keywords. For an example, look at the [Combining multiple static diagrams](combining_diagrams.ipynb) page. Moreover, you can choose the size of the figure with the `fig_size` option. You can set this parameter with a tuple containing the size in inches. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Time label \n",
    "`tulips` diagrams show stellar models at a particular moment in time. By default, the age of the star at that moment is shown in the upper left corner of the plot in units of Myr. You can hide this time label by setting `show_time_label` to `False`. It is also possible to change the location of the time label with `time_label_loc`. You can specify the location using a tuple of values that represent fractions of the maximum X and Y axis values. We can move the time label to be in the upper right corner as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=0, time_label_loc=(0.7,0.9))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Time units\n",
    "By default, the timelabel is shown in units of Myr. You can change this to another unit with the `time_unit` option. To convert the units, `tulips` makes use of `astropy.units` functionalities. Hence, the chosen unit should be a valid `astropy` unit. You can check this with `import astropy.units as u` and then press `u.`+`Tab`. This will reveal a lists with the valid units. For example, we can show the timelabel in year: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11,time_ind=0,time_unit='yr')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "    \n",
    "For information about the `time_scale_type` option, look at the [Timestep options](timestep_options.ipynb) page. \n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Setting axis limits\n",
    "It is possible to specify plot axis limits, to zoom in or out on the stellar model. In the following example, we see a circle corresponding to a stellar model whose radius represents the total mass of the star. By default, the (identical) X and Y axis limits are computed as 1.5 times the maximum of the `raxis` quantity. You can change `axis_lim` to specify a different upper limit for the X and Y axis. For example, if we set `axis_lim=10`, we are zooming in and only see the stellar model up to 10 $M_{\\odot}$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=0, axis_lim=10, raxis='star_mass')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Plotting wedges\n",
    "Instead of plotting a full circle, you can also just plot one wedge. To do this, you can specify `theta1` and `theta2` as the start and end angle (in degrees) of the wedge. This is useful when you want to combine multiple diagrams in one plot (as at the [Combining multiple static diagrams](combining_diagrams.ipynb) page). For example, we decide to plot only the first quarter as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=-1, theta1=0, theta2=90)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Hertzsprung-Russell diagram\n",
    "All `tulips` diagrams show (by default) an inset Hertzsprung-Russell diagram in the lower left corner of the plot. A blue line shows the track that the stellar model will follow during its lifetime, and a yellow circle indicates where the model is located on this track at the current time index. To hide this inset HR diagram, set `hrd_inset` to `False`. You can also show labels on the axis (log$_{10}(T_{\\textrm{eff}}/\\textrm{K})$ vs. log$_{10}(L/L_{\\odot})$) of the HR diagram by setting `show_hrd_ticks_and_labels` to `True`. \n",
    "\n",
    "##### Total mass label\n",
    "By default, `tulips` shows the total mass of the stellar object (in $M_{\\odot}$) in the bottom right corner. To hide this, you can set `show_total_mass` to `False`. \n",
    "\n",
    "##### Show stellar surface\n",
    "In a `tulips` diagram, the outer boundary of the star is shown by a black solid line. If you don't want to show this line, set `show_surface` to `False`. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tulips.perceived_color(m11, time_ind=0, show_surface=False)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Output options\n",
    "If you make an animation, it is automatically saved in a default filename in .mp4 format. However, you can change these settings. With the `output_fname` option, you can specify how you want to call the output file. Additionally, you can change the `anim_fmt` option to one of the other supported formats. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Colormap\n",
    "For all `tulips` diagrams except the perceived color diagram, it is possible to specify the colormap with the `cmap` option.  The colormap can be changed to any [matplotlib colormap](https://matplotlib.org/stable/tutorials/colors/colormaps.html), but also to [cmasher colormaps](https://cmasher.readthedocs.io/user/introduction.html), which are color-vision deficiency friendly. In the following example we load the `cmasher` library and plot an `energy_and_mixing` diagram with another colormap: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cmasher as cmr\n",
    "tulips.energy_and_mixing(m11, time_ind=2200, cmin=-10, cmax=10, show_total_mass=True,\n",
    "                         cmap='cmr.rainforest')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Options available for specific TULIPS diagrams\n",
    "\n",
    "In addition to these general options, there are more possibilities specific for each `tulips` diagram. These are explained in the page of the corresponding diagram, which you can access via the navigation menu. "
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Metagegevens bewerken",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
