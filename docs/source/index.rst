.. image:: ../../tulips/logo/Tulips_acronym_text_transparent.png

Documentation
=============



Welcome to the ``tulips`` documentation! The ``tulips`` python package creates visualizations of stars
based on output from the `MESA <http://mesa.sourceforge.net/>`_ stellar evolution code. TULIPS represents stars as
circles with varying size and color. Click the links below to learn more about TULIPS through examples,
tutorials, and detailed documentation.

.. raw:: html

    <div class="info">
        <p><strong>News</strong> The TULIPS paper has just been <a href=https://doi.org/10.1016/j.ascom.2021.100516>published</a>. With it, example MESA models
        are now available for download on zenodo <a href="https://doi.org/10.5281/zenodo.5032250"><img src="https://zenodo.org/badge/DOI/10.5281/zenodo.5032250.svg" alt="DOI"></a>
        .</p>
    </div>

.. image:: figures/first_animation.gif
    :align: center

.. toctree::
    :maxdepth: 1
    :caption: Contents
    :hidden:

    installation
    citing_tulips
    getting_started

.. toctree::
    :maxdepth: 1
    :caption: Tulips diagrams
    :hidden:

    perceived_color_diagram
    energy_and_mixing_diagram
    property_profile_diagram
    chemical_profile_diagram
    combining_diagrams

.. toctree::
    :maxdepth: 1
    :caption: Additional options
    :hidden:

    customize
    timestep_options

.. toctree::
    :hidden:
    :caption: The TULIPS code

    api

.. toctree::
    :maxdepth: 1
    :caption: External links
    :hidden:

    The TULIPS repository <https://bitbucket.org/elaplace/tulips/src/master/>
    TULIPS entry on ASCL <https://ascl.net/2110.004>
    Download example MESA models <https://doi.org/10.5281/zenodo.5032250>

.. raw:: html

    <div class="nav-container" style="margin-bottom:30px;">
        <div class="box" data-href="installation.html">Installation</div>
        <div class="box" data-href="citing_tulips.html">Citing TULIPS</div>
        <div class="box" data-href="getting_started.html">Getting Started</div>
    </div>

Tulips diagrams
===============

.. raw:: html

    <div class="nav-container" style="margin-bottom:30px;">
        <div class="box" data-href="perceived_color_diagram.html">Perceived color diagram</div>
        <div class="box" data-href="energy_and_mixing_diagram.html">Energy and mixing diagram</div>
        <div class="box" data-href="property_profile_diagram.html">Property profile diagram</div>
    </div>

    <div class="nav-container" style="margin-bottom:30px;">
        <div class="box" data-href="chemical_profile_diagram.html">Chemical profile diagram</div>
        <div class="box" data-href="combining_diagrams.html">Combining diagrams</div>
    </div>

Additional options
==================

.. raw:: html

    <div class="nav-container">
        <div class="box" data-href="timestep_options.html">Timestep options</div>
        <div class="box" data-href="customize.html">Customize options</div>
    </div>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Contributing
============
If you wish to submit a new feature or bug fix please send a pull request.

Acknowledgments
===============
tulips makes use of the open-source python modules mesaPlot (`Farmer <https://zenodo.org/record/3515246#.YO7m6egzZPY>`_,
2019), matplotlib (`Hunter <https://ui.adsabs.harvard.edu/abs/2007CSE.....9...90H/abstract>`_, 2007), numpy (`van der
Walt et al. <https://ui.adsabs.harvard.edu/abs/2011CSE....13b..22V/abstract>`_, 2011), Colorpy (`Mark Kness <http://markkness.net/colorpy/ColorPy.html>`_),
astropy (`Astropy Collaboration et al. <https://www.aanda.org/articles/aa/full_html/2013/10/aa22068-13/aa22068-13.html>`_, 2013, 2018),
CMasher (`van  der  Velden <https://joss.theoj.org/papers/10.21105/joss.02004>`_,  2020), and ipython/jupyter
(`Perez and Granger <https://ui.adsabs.harvard.edu/abs/2007CSE.....9c..21P/abstract>`_, 2007). Logo design: A. Faber.
Documentation: I. de Langen. Created and developed by `E. Laplace <https://evalaplace.github.io/>`_ .

Funding
=======
This project was funded by the European Unions Horizon 2020
research and innovation program from the European Research
Council (ERC, grant agreement No.715063) and the Netherlands
Organisation for Scientific Research (NWO) as part of the Vidi
research program BinWaves with project number 639.042.728.
The `ET Outreach award <https://khmw.nl/et-outreach-award/>`_ of the Royal Holland Society of Sciences and Humanities and its
sponsors E. van Dishoek and T. de Zeeuw are gratefully acknowledged for
making it possible to fund the work of I. de Langen on the TULIPS
documentation and tutorials.

